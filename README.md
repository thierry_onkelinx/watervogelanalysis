**Branch** | **Build status** | **Test coverage**
-- | -- | --
master | [![wercker status](https://app.wercker.com/status/15684e165700dc3621894367c7bc302e/s/master "wercker status")](https://app.wercker.com/project/bykey/15684e165700dc3621894367c7bc302e) | [![codecov.io](https://codecov.io/bitbucket/thierry_onkelinx/watervogelanalysis/coverage.svg?branch=master)](https://codecov.io/bitbucket/thierry_onkelinx/watervogelanalysis?branch=master)
develop | [![wercker status](https://app.wercker.com/status/15684e165700dc3621894367c7bc302e/s/develop "wercker status")](https://app.wercker.com/project/bykey/15684e165700dc3621894367c7bc302e) | [![codecov.io](https://codecov.io/bitbucket/thierry_onkelinx/watervogelanalysis/coverage.svg?branch=develop)](https://codecov.io/bitbucket/thierry_onkelinx/watervogelanalysis?branch=develop)

# The watervogelanalysis package

The `watervogelanalysis` package imports the data from the Flemish and Walloon monitoring schemes of the wintering birds. Prepares analysis datasets and runs the analysis.
